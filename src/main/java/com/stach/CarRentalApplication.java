package com.stach;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

//import com.stach.dao.CarDAO;
import com.stach.service.ApplicationService;

@SpringBootApplication
public class CarRentalApplication {
	
	@Bean
	Scanner scanner() {
		return new Scanner(System.in);
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx=SpringApplication.run(CarRentalApplication.class, args);
		
		ApplicationService controller=ctx.getBean(ApplicationService.class);
		controller.executeOption();			
	}
}
