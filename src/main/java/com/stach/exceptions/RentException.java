package com.stach.exceptions;

public class RentException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public RentException(String message) {
		super(message);
	}
}
