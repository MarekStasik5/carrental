package com.stach.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stach.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>  {
	Optional<Category> findByNameIgnoreCase(String name);
	boolean findByName(String name);
	Long findIdByName(String name);

}
