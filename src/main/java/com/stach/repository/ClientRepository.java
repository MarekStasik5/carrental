package com.stach.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stach.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long>  {
	
	void deleteById(Long id);

}
