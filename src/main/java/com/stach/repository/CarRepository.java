package com.stach.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stach.model.Car;

public interface CarRepository extends JpaRepository<Car, Long> {
	List<Car> findAllByNameContainingIgnoreCase(String name);
		
}
