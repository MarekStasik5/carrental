package com.stach.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.stach.model.Car;

@Repository
public class CarDAO {
	
	@PersistenceContext
	private EntityManager entityMenager;
	
	@Transactional
	public Car save(Car car) {
		entityMenager.persist(car);
		return car;
	}
	
	public Car read(Long id) {
		return entityMenager.find(Car.class, id);
	}
	
	@Transactional
	public Car update(Car car) {
		return entityMenager.merge(car);
	}
	
	@Transactional
	public void delete(Car car) {
		Car attachedCar=read(car.getId());
		entityMenager.remove(attachedCar);
	}

}
