package com.stach.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.stach.model.Category;

@Repository
public class CategoryDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public Category save(Category category) {
		entityManager.persist(category);
		return category;
	}
	
	public Category read(Long id) {
		return entityManager.find(Category.class, id);
	}
	
	@Transactional
	public Category update(Category category) {
		return entityManager.merge(category);
	}
	
	@Transactional
	public void delete(Category category) {
		Category attachedCaegory=read(category.getId());
		entityManager.remove(attachedCaegory);
	}

}
