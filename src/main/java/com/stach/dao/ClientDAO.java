package com.stach.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.stach.model.Client;

@Repository
public class ClientDAO {

	@PersistenceContext
	private EntityManager entityMenager;
	
	@Transactional
	public Client save(Client client) {
		entityMenager.persist(client);
		return client;
	}
	
	public Client read(Long id) {
		return entityMenager.find(Client.class, id);
	}
	
	@Transactional
	public Client update(Client client) {
		return entityMenager.merge(client);
	}
	
	@Transactional
	public void delete(Client client) {
		Client attachedClient=read(client.getId());
		entityMenager.remove(attachedClient);
	}
	
}
