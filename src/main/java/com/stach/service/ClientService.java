package com.stach.service;

import java.util.Optional;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stach.model.Client;
import com.stach.repository.ClientRepository;

@Service
public class ClientService {
	
	private Scanner scanner;
	private ClientRepository clientRespository;
	
	@Autowired
	public ClientService(Scanner scanner, ClientRepository clientRespository) {
		this.scanner = scanner;
		this.clientRespository = clientRespository;
	}
	
	private Client readClient() {
		Client client=new Client();
		System.out.println("Enter first name: ");
		client.setFirstName(scanner.next());
		System.out.println("Enter last name: ");
		client.setLastName(scanner.next());
		System.out.println("Enter PESEL: ");
		client.setPesel(scanner.nextLong());
		return client;
	}

	public void createClient() {
		Client client = readClient();
		clientRespository.save(client);
		System.out.println("Client added");
	}
	
	public void removeClent() {
		System.out.println("Enter client's id");
		Long id=scanner.nextLong();
		Optional<Client>client=clientRespository.findById(id);
		client.ifPresentOrElse(clientRespository::delete,()->System.out.println("Client doesn;t exist"));
	}
	
}
