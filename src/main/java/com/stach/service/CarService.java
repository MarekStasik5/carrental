package com.stach.service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stach.exceptions.CategoryNotFoundException;
import com.stach.model.Car;
import com.stach.model.Category;
import com.stach.repository.CarRepository;
import com.stach.repository.CategoryRepository;

@Service
@Transactional
public class CarService {
	
	private Scanner scanner;
	private CarRepository carReposiory;
	private CategoryRepository categoryRepository;
	
	@Autowired
	public CarService(Scanner scanner, CarRepository carReposiory, CategoryRepository categoryRepository) {
		this.scanner = scanner;
		this.carReposiory = carReposiory;
		this.categoryRepository = categoryRepository;
	}
	
	public Car readCar() {
		Car car=new Car();
		System.out.println("Enter name: ");
		car.setName(scanner.next());
		System.out.println("Enter description: ");
		car.setDescription(scanner.next());		
		System.out.println("Enter quantity: ");
		car.setQuantity(scanner.nextInt());
		System.out.println("Enter category name: ");
		String categoryName=scanner.next();
		Optional<Category>category=categoryRepository.findByNameIgnoreCase(categoryName);
		category.ifPresentOrElse(car::setCategory,()->{throw new CategoryNotFoundException("Category doesn't exist");});
		return car;
	}

	public void addNewCar() {
		try {
			Car car=readCar();
			carReposiory.save(car);
			System.out.println("Car added");
		}catch(CategoryNotFoundException e) {
			System.out.println("The car has not been added");
		}
	}
	
	public void removeCar() {	
		System.out.println("Enter car id to remowe: ");
		Long id = scanner.nextLong();;
		Optional<Car>car=carReposiory.findById(id);
		car.ifPresentOrElse(carReposiory::delete, ()-> System.out.println("Car doesn't exist"));	
	}
	
	public void searchCar() {
		System.out.println("Enter part of car name");
		String name=scanner.next();
		List<Car>car=carReposiory.findAllByNameContainingIgnoreCase(name);
		if(car.isEmpty())
			System.out.println("Result no found");
		else {
			System.out.println("Car found");
			car.forEach(System.out::println);
		}
	}
		
}
