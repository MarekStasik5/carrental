package com.stach.service;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ApplicationService {
	
	private CarService carService;
	private CategoryService categoryService;
	private ClientService clientService;
	private RentService rentservice;
	private Scanner scanner;
	
	@Autowired
	public ApplicationService(CarService carService, CategoryService categoryService, ClientService clientService,
			RentService rentservice, Scanner scanner) {
		this.carService = carService;
		this.categoryService = categoryService;
		this.clientService = clientService;
		this.rentservice = rentservice;
		this.scanner = scanner;
	}

	public void menu() {
		System.out.println("1. Add car");
		System.out.println("2. Add category");
		System.out.println("3. Add client");
		System.out.println("4. Rent car");
		System.out.println("5. Delete car");
		System.out.println("6. Delete category");
		System.out.println("7. Delete client");
		System.out.println("8. Search car");
		System.out.println("9. Close application");	
		System.out.println("Choose option");
	}
	
	public void executeOption() {
		int option=0;
		while(option!=8)
		{
			menu();
			option=scanner.nextInt();
			switch(option) {
				case 1:
					carService.addNewCar();
					break;
				case 2:
					categoryService.addCategory();
					break;
				case 3:
					clientService.createClient();
					break;
				case 4:
					rentservice.rentCar();
					break;
				case 5:
					carService.removeCar();
					break;
				case 6:
					categoryService.removecategory();
					break;
				case 7:
					clientService.removeClent();
					break;
				case 8:
					carService.searchCar();
					break;
				case 9:
					scanner.close();
					break;
				default :
					menu();					
				
			}
			
		}
		
	}

}
