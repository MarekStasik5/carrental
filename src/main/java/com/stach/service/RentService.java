package com.stach.service;

import java.util.Optional;
import java.util.Scanner;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stach.exceptions.RentException;
import com.stach.model.Car;
import com.stach.model.Client;
import com.stach.repository.CarRepository;
import com.stach.repository.ClientRepository;

@Service
@Transactional
public class RentService {
	
	private Scanner scanner;
	private CarRepository carRepository;
	private ClientRepository clientRepository;
		
	@Autowired
	public RentService(Scanner scanner, CarRepository carRepository, ClientRepository clientRepository) {
		this.scanner = scanner;
		this.carRepository = carRepository;
		this.clientRepository = clientRepository;
	}
	
	public void rentCar() {
		System.out.println("Enter client id");
		Long clientId=scanner.nextLong();
		System.out.println("Enter car id");
		Long carId=scanner.nextLong();
		Optional<Client>client=clientRepository.findById(clientId);
		Optional<Car>car=carRepository.findById(carId);
		if(client.isPresent())
            car.ifPresentOrElse(dev -> {
                if(dev.getQuantity() > dev.getClients().size())
                    dev.addClient(client.get());
                else
                    throw new RentException("All deviced rented");
            }, () -> {
                throw new RentException("Device doesn't exist");
            });
        else
            throw new RentException("Client doesn't exist");
	}

	
}
