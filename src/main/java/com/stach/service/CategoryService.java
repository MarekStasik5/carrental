package com.stach.service;

import java.util.Optional;
import java.util.Scanner;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.stach.model.Category;
import com.stach.repository.CategoryRepository;

@Service
@Transactional
public class CategoryService {
	
	private CategoryRepository categoryReposiotry;
	private Scanner scanner;
	
	@Autowired	
	public CategoryService(CategoryRepository categoryReposiotry, Scanner scanner) {
		this.categoryReposiotry = categoryReposiotry;
		this.scanner = scanner;
	}
	
	private Category readCategory() {
		Category category=new Category();
		System.out.println("Enter name: ");
		category.setName(scanner.next());
		System.out.println("Enter descrition: ");
		category.setDescription(scanner.next());
		return category;
	}

	public void addCategory() {
		Category category=readCategory();
		try {
			categoryReposiotry.save(category);
			System.out.println("Category added");
		}catch(DataIntegrityViolationException e) {
			System.out.println("Failed. Category exist!");
		}
	}
	
	public void removecategory() {
		System.out.println("Enter category ID: ");
		long categoryId=scanner.nextLong();
		Optional<Category>category=categoryReposiotry.findById(categoryId);
		category.ifPresentOrElse(categoryReposiotry::delete, ()->System.out.println("Category doesn't exist"));			
	}


}
